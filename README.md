# Simple Queues
## Node.js, Redis, Beanstalkd, Mongo, Artillery

## Run

- ```git clone https://gitlab.com/sytn1kk/simple-queues.git```
- ```cd simple-queues```
- ```docker-compose -f "docker-compose.yml" up -d --build```
- ```sh ./speed-test/speed-test.sh```

## Rps

```
All VUs finished. Total time: 1 minute, 41 seconds

--------------------------------
Summary report @ 18:44:03(+0000)
--------------------------------

vusers.created_by_name.0: ................................... 29993
vusers.created.total: ....................................... 29993
vusers.completed: ........................................... 29993
vusers.session_length:
  min: ...................................................... 3
  max: ...................................................... 1013.4
  median: ................................................... 68.7
  p95: ...................................................... 645.6
  p99: ...................................................... 820.7
http.request_rate: .......................................... 324/sec
http.requests: .............................................. 29993
http.codes.200: ............................................. 29993
http.responses: ............................................. 29993
http.response_time:
  min: ...................................................... -27
  max: ...................................................... 997
  median: ................................................... 18
  p95: ...................................................... 620.3
  p99: ...................................................... 804.5
[+] Building 0.9s (9/9) FINISHED  
```

## Test results

- Avg redis-rdb time in ms: 11998
- Avg redis-aof time in ms: 12549
- Avg beanstalkd time in ms: 6172

## Screenshots
![Explain without index](https://i2.paste.pics/f5f4ac00032c13a70a794448ed435752.png "Explain without index")

