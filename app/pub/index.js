const fastify = require('fastify');

const { init, send } = require('./queues')

const server = fastify({
    logger: true
})

server.post('/pub', async (request, reply) => {
    await send(request.body)
    return 'OK'
})


const main = async () => {
    try {
        await init()
        await server.listen(3000, '0.0.0.0')
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
}

main()