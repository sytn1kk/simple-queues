const { Client } = require('node-beanstalk');

const beanstalkdClient = new Client({
    host: process.env.BEANSTALKD_HOST,
    port: Number(process.env.BEANSTALKD_PORT)
})

const connect = async tube => {
    await beanstalkdClient.connect()
    await beanstalkdClient.use(tube)
}

const beanstalkdInit = async tube => {
    await connect(tube)
}

module.exports = {
    beanstalkdClient,
    beanstalkdInit,
}