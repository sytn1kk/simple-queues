const Queue = require('bee-queue')

const { beanstalkdInit, beanstalkdClient } = require('./beanstalk')

const TUBE = 'SPEED_TEST'

const redisAOF = new Queue(TUBE, {
    redis: {
        host: process.env.REDIS_AOF_HOST,
        port: process.env.REDIS_AOF_PORT
    }
})

const redisRDB = new Queue(TUBE, {
    redis: {
        host: process.env.REDIS_EDB_HOST,
        port: process.env.REDIS_EDB_PORT
    }
})

const init = async () => {
    await beanstalkdInit(TUBE)
}

const send = async body => {
    const message = {
        created: new Date().toISOString(),
        body
    }
    await Promise.all([
        beanstalkdClient.put(message),
        redisAOF.createJob(message).save(),
        redisRDB.createJob(message).save()
    ])
}

module.exports = { 
    init,
    send,
}