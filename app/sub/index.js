const { connect } = require('./message')

const { listenBeanstalk } = require('./queues/beanstalk')
const { listenRedisAOF } = require('./queues/redis-aof')
const { listenRedisRDB } = require('./queues/redis-rdb')

const TUBE = 'SPEED_TEST'

const listen = async () => {
    await connect()
    listenBeanstalk(TUBE)
    listenRedisAOF(TUBE)
    listenRedisRDB(TUBE)
}

listen()