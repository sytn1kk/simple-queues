const { save } = require('../../message')

const { Client } = require('node-beanstalk')

const beanstalkdClient = new Client({
    host: process.env.BEANSTALKD_HOST,
    port: Number(process.env.BEANSTALKD_PORT)
});

const connect = async tube => {
    await beanstalkdClient.connect()
    await beanstalkdClient.use(tube)
}

const listen = async tube => {
    await connect(tube)

    await beanstalkdClient.watch(tube)

    while (true) {
        const message = await beanstalkdClient.reserve()
        if (message) {
            message.payload.received = new Date().toISOString()
            await save('beanstalkd', message.payload)
            await beanstalkdClient.delete(message.id)
        }
        
    }
}

module.exports = { listenBeanstalk: listen }