const { save } = require('../../message')

const Queue = require('bee-queue')

const listenRedisRDB = tube => {
    const queue = new Queue(tube, {
        redis: {
            host: process.env.REDIS_EDB_HOST,
            port: process.env.REDIS_EDB_PORT
        }
    })

    queue.process(async ({ data }, done) => {    
        data.received = new Date().toISOString()
        await save('redis-rdb', data)
        done(null)
    })
}

module.exports = { listenRedisRDB }