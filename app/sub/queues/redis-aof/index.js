const { save } = require('../../message')

const Queue = require('bee-queue')

const listenRedisAOF = tube => {
    const queue = new Queue(tube, {
        redis: {
            host: process.env.REDIS_AOF_HOST,
            port: process.env.REDIS_AOF_PORT
        }
    })

    queue.process(async ({ data }, done) => {
        data.received = new Date().toISOString()
        await save('redis-aof', data)
        done(null)
    })
}

module.exports = { listenRedisAOF }