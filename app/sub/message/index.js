const { MongoClient } = require('mongodb')

const client = new MongoClient(process.env.MONGO_URL)

const connect = async () => {
    await client.connect()
}

const save = async (collectionName, message) => {
    const db = client.db('SPEED_TEST')
    const collection = db.collection(collectionName)

    await collection.insertOne(message)
}

module.exports = { connect, save }