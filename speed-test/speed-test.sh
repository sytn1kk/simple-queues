#!/bin/bash

docker run --network=simple-queues_simple-queues --rm -it -v "$(pwd)/artillery/":/scripts artilleryio/artillery:latest run /scripts/load-testing.yml

docker build -t speed-test . && docker run --network=simple-queues_simple-queues --env-file "$(pwd)/../configuration.env" -it speed-test