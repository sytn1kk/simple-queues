const moment = require('moment')
const { MongoClient } = require('mongodb')

const client = new MongoClient(process.env.MONGO_URL)

const db = client.db('SPEED_TEST')

const getQueueAvarageTime = async collectionName => {
    const collection = db.collection(collectionName)
    const messages = await collection.find().toArray()
    const diffSum = messages.reduce((acc, message) => {
        const diffInMs = moment(message.received).diff(moment(message.created))
        return acc + diffInMs
    }, 0)
    return Math.round(diffSum / messages.length)
}

const main = async () => {
    await client.connect()

    const [ redisRdb, redisAof, beanstalkd ] = await Promise.all([
        getQueueAvarageTime('redis-rdb'),
        getQueueAvarageTime('redis-aof'),
        getQueueAvarageTime('beanstalkd'),
    ])

    console.log(`Avg redis-rdb time in ms: ${redisRdb}`)
    console.log(`Avg redis-aof time in ms: ${redisAof}`)
    console.log(`Avg beanstalkd time in ms: ${beanstalkd}`)

    process.exit(0)
}

main()